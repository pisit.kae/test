const webUrl = "http://localhost:8080//";
const web =
  webUrl +
  "account/handle?auth=hpJWEQkq3dZN7tQF+EUArXWBOxptwaWLa3lnhu7Jsynsg6ep2NiZqd9sNZrQdYFbAolBMPD8AjVw5PkMhci+T9GF3op8TZHBzcwCqhgHMOJsGEFQiBm2Xa1b8eR8NRBe2Fax/RVQ5wHv5v6sKRqFTBje92kVM1H6y2OdWaOfAIp0NepVRuIt8j3xUciFmy8Vdbu52IGpOHLEl9gAaF3i1CyTCRyhWDk7IIUnx8XI+4sPwimuqy8PJkTap+FF+dR4B1gk54iuyhP+WYM+wLB46IDS9rHIC74lzTw9kHPYNRxWq535KViDum9IIxtFChizRvSojv1Ms+QhlpJ7tLNJew==";

describe("Web Shipping", () => {
  const testCreateDeimension = (e) => {
    cy.log(JSON.stringify(e));

    if (e.productNameTH) {
      cy.get('input[name="productNameTH"]').type(e.productNameTH);
    }

    if (e.productNameEN) {
      cy.get('input[name="productNameEN"]').type(e.productNameEN);
    }

    if (e.productNameCH) {
      cy.get('input[name="productNameCH"]').type(e.productNameCH);
    }

    if (e.brandId) {
      cy.get(".ant-select-selector").eq(0).click();
      cy.contains(e.brandId).click();
    }

    if (e.categoryId) {
      cy.get(".ant-select-selector").eq(1).click();
      cy.contains(e.categoryId).click();
    }

    if (e.productDescription) {
      cy.get('textarea[name="productDescription"]').type(e.productDescription);
    }

    if (e.instruction) {
      cy.get('textarea[name="instruction"]').type(e.instruction);
    }

    if (e.material) {
      cy.get('textarea[name="material"]').type(e.material);
    }

    if (e.dimensions) {
      cy.get('input[name="dimensions"]').type(e.dimensions);
    }

    if (e.productWeight) {
      cy.get('input[name="productWeight"]').type(e.productWeight);
    }

    if (e.unitWeight) {
      cy.get('input[name="unitWeight"]').type(e.unitWeight);
    }

    cy.contains("บันทึกแบบร่าง").click();
  };

  it("Test Creat Dimension valid", () => {
    cy.visit(web); //เข้าเว็บ
    cy.contains("Web Shipping"); //หา text
    cy.visit(webUrl + "dimensionDetail");
    cy.readFile("./cypress/files/TestCase - valid.csv") //อ่านไฟล์
      .then((data) => {
        cy.task("csvToJson", data).then((data) => {
          //แปลงค่าไฟล์ที่อ่านมา (.csv) ให้เป็น Object โดยใข้ plusin (function นอก)
          data.forEach((e) => {
            testCreateDeimension(e);
            cy.url().should("include", e.url); //เช็ค url
            cy.contains(e.output);
            cy.visit(webUrl + "dimensionDetail");
          });
          cy.log(data);
          //cy.get('input')
          //    .eq(0)
          //    .type(data['productNameTH'])
        });
      });
  });

  it("Test Creat Dimension Invalid", () => {
    cy.visit(web); //เข้าเว็บ
    cy.contains("Web Shipping"); //หา text
    cy.visit(webUrl + "dimensionDetail");
    cy.readFile("./cypress/files/TestCase - invalid.csv").then((data) => {
      cy.task("csvToJson", data).then((data) => {
        data.forEach((e) => {
          testCreateDeimension(e);
          cy.url().should("include", e.url);
          cy.contains(e.output);
          cy.visit(webUrl + "dimensionDetail");
        });
        cy.log(data);
        //cy.get('input')
        //    .eq(0)
        //    .type(data['productNameTH'])
      });
    });
  });
});
