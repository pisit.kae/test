const webUrl = 'http://localhost:8080/';
const webInit = webUrl + 'account/handle?auth=hpJWEQkq3dZN7tQF+EUArXWBOxptwaWLa3lnhu7Jsynsg6ep2NiZqd9sNZrQdYFbAolBMPD8AjVw5PkMhci+T9GF3op8TZHBzcwCqhgHMOJsGEFQiBm2Xa1b8eR8NRBe2Fax/RVQ5wHv5v6sKRqFTBje92kVM1H6y2OdWaOfAIp0NepVRuIt8j3xUciFmy8Vdbu52IGpOHLEl9gAaF3i1CyTCRyhWDk7IIUnx8XI+4sPwimuqy8PJkTap+FF+dR4B1gk54iuyhP+WYM+wLB46IDS9rHIC74lzTw9kHPYNRxWq535KViDum9IIxtFChizRvSojv1Ms+QhlpJ7tLNJew=='


describe('Web Shipping', () => {

    const trackingStatusCreate = (e) => {
        cy.get("button:visible").contains('เพิ่ม')
            .click()
        cy.get('input[name="trackingStatusID"]')
            .type(e.id)
        cy.get('input[name="trackingStatusDescription"]')
            .type(e.description)
        cy.contains("ยืนยัน")
            .click()
        cy.url().should('include', e.url);
        cy.contains(e.output);
    }

    const trackingStatusUpdate = (e) => {
        cy.contains(e.id).parents('tr').within(() => { //หา element ระหว่าง <tr> </tr> นั้น ๆ
            cy.get('td').eq(4).contains("แก้ไข").click(); //เลือกหา td ลำดับที่ 5 (array[4])
        })
        cy.get('input[name="trackingStatusID"]')
            .type('{selectAll}').type(e.newID) //คลุมดำทั้ง input นั้นเพื่อแทนค่า
        cy.get('input[name="trackingStatusDescription"]')
            .type('{selectAll}').type(e.newDescription)
        cy.get("button:visible").contains("ยืนยัน").click();
        cy.url().should('include', e.url);
        cy.contains(e.output);
    }

    const detailStatusUpdate = (e) => {
        cy.contains(e.detailStatus).parents('tr').within(() => {
            cy.get('td').eq(4).contains("แก้ไข").click();
        })
        cy.get('input[name="detailStatus"]')
            .type('{selectAll}').type(e.newDetailStatus)
        cy.get("div:visible").get('.ant-select-selector').eq(2).click()
        cy.contains(e.newTrackingStatus)
            .click()
        cy.get("button:visible").contains("ยืนยัน")
            .click()
        cy.url().should('include', e.url);
        cy.contains(e.output);
    }

    const detailStatusCreate = (e) => {
        cy.get("button:visible").contains('เพิ่ม') //หาเฉพาะปุ่มที่แสดง
            .click()
        cy.get('input[name="detailStatus"]')
            .type(e.detailStatus)
        cy.get("div:visible").get('.ant-select-selector').eq(2).click()
        cy.contains(e.trackingStatus)
            .click()
        cy.get("button:visible").contains("ยืนยัน")
            .click()
        cy.url().should('include', e.url);
        cy.contains(e.output);
    }


    const shippingStatusUpdate = (e) => {
        cy.contains(e.description).parents('tr').within(() => {
            cy.get('td').eq(3).contains("แก้ไข").click();
        })
        cy.get('input[name="shippingStatusDescription"]')
            .type('{selectAll}').type(e.newDescription)
        cy.get("button:visible").contains("ยืนยัน")
            .click()
        cy.url().should('include', e.url);
        cy.contains(e.output);
    }

    const shippingStatusCreate = (e) => {
        cy.get("button:visible").contains('เพิ่ม')
            .click()
        cy.get('input[name="shippingStatusDescription"]')
            .type(e.description)
        cy.get("button:visible").contains("ยืนยัน")
            .click()
        cy.url().should('include', e.url);
        cy.contains(e.output);
    }

    const deleteData = (e, content, eq = 4) => {
        cy.contains(content).parents('tr').within(() => {
            cy.get('td').eq(eq).contains("ลบ").click();
        })
        cy.wait(1000)
        cy.get("button:visible").contains("ยืนยัน").click();
        cy.contains(e.output);
    }

    const activateData = (e, content, eq = 3) => {
        cy.contains(content).parents('tr').within(() => { //find only in specific tr
            cy.get('td').eq(eq).contains("ปิดใช้งาน").click();
        })
        cy.contains(e.output);
    }

    const deactivateData = (e, content, eq = 3) => {
        cy.contains(content).parents('tr').within(() => { //find only in specific tr
            cy.get('td').eq(eq).contains("เปิดใช้งาน").click();
        })
        cy.contains(e.output);
    }

    it('Test Tracking Status', () => {
        cy.visit(webInit); //เข้าเว็บ
        cy.contains('Web Shipping'); //หา text
        cy.visit(webUrl + 'shipmentMaster');
        cy.readFile('./cypress/files/TestCase - TrackingStatus.csv')
            .then((data) => {
                cy.task('csvToJson', data).then((data) => {
                    cy.log(JSON.stringify(data));
                    data.forEach(e => {
                        switch (e.testCase) {
                            case 'create': trackingStatusCreate(e); break;
                            case 'update': trackingStatusUpdate(e); break;
                            case 'deactivate': deactivateData(e, e.id); break;
                            case 'activate': activateData(e, e.id); break;
                            case 'delete': deleteData(e, e.id); break;
                        }
                    });
                })
            })
    })

    it('Test Detail Status', () => {
        cy.visit(webInit); //เข้าเว็บ
        cy.contains('Web Shipping'); //หา text
        cy.visit(webUrl + 'shipmentMaster');
        cy.contains("Detail Status").click();
        cy.readFile('./cypress/files/TestCase - Detail Status.csv')
            .then((data) => {
                cy.task('csvToJson', data).then((data) => {
                    cy.log(JSON.stringify(data));
                    data.forEach(e => {
                        switch (e.testCase) {
                            case 'create': detailStatusCreate(e); break;
                            case 'update': detailStatusUpdate(e); break;
                            case 'deactivate': deactivateData(e, e.detailStatus); break;
                            case 'activate': activateData(e, e.detailStatus); break;
                            case 'delete': deleteData(e, e.detailStatus); break;
                        }
                    });
                    cy.log(data)
                    //cy.get('input')
                    //    .eq(0)
                    //    .type(data['productNameTH'])
                })
            })
    })

    it('Test Shipping Status', () => {
        cy.visit(webInit); //เข้าเว็บ
        cy.contains('Web Shipping'); //หา text
        cy.visit(webUrl + 'shipmentMaster');
        cy.contains("Shipping Status").click();
        cy.readFile('./cypress/files/TestCase - Shipping Status.csv')
            .then((data) => {
                cy.task('csvToJson', data).then((data) => {
                    cy.log(JSON.stringify(data));
                    data.forEach(e => {
                        switch (e.testCase) {
                            case 'create': shippingStatusCreate(e); break;
                            case 'update': shippingStatusUpdate(e); break;
                            case 'deactivate': deactivateData(e, e.description, 2); break;
                            case 'activate': activateData(e, e.description, 2); break;
                            case 'delete': deleteData(e, e.description, 3); break;
                        }
                    });
                    cy.log(data)
                })
            })
    })
})
